"""
Module that find duplicated wav files.

Author: @1jorge.echeverria
sys.argv[1]: Folder 1.
sys.argv[2]: Folder 2.
"""

import sys
import os
import pathlib
import itertools
import numpy as np
import soundfile as sf


def __is_wav_file(file):
    """
    Check if the file is a .wav file.

    Parameters:
        file (str): File's full path.

    Returns:
        bool: True if file is .wav.
    """
    return pathlib.Path(file).suffix.lower() == '.wav'


def __have_other_audio_formats(file):
    """
    Check if the file is a .mp3, .aif, .aiff, .wv, .aac, .flac, .m4a, .ogg or .wma file.

    Parameters:
        file (str): File's full path.

    Returns:
        bool: True if file is .mp3, .aif, .aiff, .wv, .aac, .flac, .m4a, .ogg or .wma.
    """
    return pathlib.Path(file).suffix.lower() in ['.mp3', '.aif', '.aiff', '.wv', '.aac', '.flac', '.m4a', '.ogg', '.wma']


def __get_file_list(folder):
    """
    Creates a list with all files from folder.

    Parameters:
        folder (str): Folder's full path.

    Returns:
        list: Supported .wav files.
    """
    file_list = []
    unsupported_audio_files = []
    for dirpath, _, filenames in os.walk(folder):
        for filename in filenames:
            # Get file's full path.
            file = os.path.join(dirpath, filename)
            try:
                # Normalices file's path.
                file = os.path.normpath(file)
                # Add path to list.
                if __is_wav_file(file):
                    file_list.append(file)
                elif __have_other_audio_formats(file):
                    unsupported_audio_files.append(file)
            except UnicodeDecodeError:
                pass
    if unsupported_audio_files:
        print('\nFILES WITH OTHER EXTENSIONS:\n')
        for file1 in unsupported_audio_files:
            print(file1)
    return file_list


def __null_test(file1, file2, wav_32float):
    """
    Verify if 2 audio files are equal.

    Parameters:
        file1 (str):
        file2 (str):
        wav_32float (list):

    Returns:
        bool:
    """
    try:
        # Cargar los archivos WAV
        data1, _ = sf.read(file1, dtype='int16')
        data2, _ = sf.read(file2, dtype='int16')
    except sf.LibsndfileError as e:
        name = e.args[1].split("'")[1].replace("\\\\", "\\")
        if not name in wav_32float:
            wav_32float.append(name)
            print(f"{name}: Unsupported WAV")
    else:
        # Convertir los datos de audio a punto flotante y normalizarlos a 0 dB
        data1_norm = data1.astype(float) / np.max(np.abs(data1))
        data2_norm = data2.astype(float) / np.max(np.abs(data2))

        # Convertir los archivos de audio a mono si tienen más de un canal
        if len(data1_norm.shape) > 1:
            data1_norm = np.mean(data1_norm, axis=1)
        if len(data2_norm.shape) > 1:
            data2_norm = np.mean(data2_norm, axis=1)

        # Asegurarse de que los dos arrays tengan la misma longitud
        min_len = min(len(data1_norm), len(data2_norm))
        data1_norm = data1_norm[:min_len]
        data2_norm = data2_norm[:min_len]

        # Calcular la diferencia absoluta entre los archivos normalizados
        diff = np.abs(data1_norm - data2_norm)

        # Calcular la energía de la diferencia
        energy_diff = np.sum(diff ** 2)

        # Comparar la energía de la diferencia con un umbral para determinar si hay nulidad
        umbral = 1e-10  # Umbral de energía para considerar nulidad
        return energy_diff < umbral


def __check_equality(file1, file2, wav_32float):
    """
    Verify if 2 files are equals using null test.

    Parameters:
        file1 (str):
        file2 (str):
        wav_32float (list):
    """
    if __null_test(file1, file2, wav_32float):
        print(f"{str(file1)} vs {str(file2)} : Duplicated WAV")


def __search_into_folder(folder):
    """
    Creates the combinations needed to do null test with all files in 1 folder.

    Parameters:
        folder (str):
    """
    wav_32float = []
    print('\nANALYZED FILES: \n')
    for file1, file2 in itertools.combinations(folder, 2):
        __check_equality(file1, file2, wav_32float)


def __compare_two_folders(folder1, folder2):
    """
    Creates the combinations needed to do null test with all files in 2 folders.

    Parameters:
        folder1 (str):
        folder2 (str):
    """
    wav_32float = []
    print('\nANALYZED FILES: \n')
    for file1, file2 in itertools.product(folder1, folder2):
        __check_equality(file1, file2, wav_32float)


if __name__ == "__main__":

    if len(sys.argv) == 3:
        __compare_two_folders(
            __get_file_list(sys.argv[1]),
            __get_file_list(sys.argv[2])
        )
    else:
        __search_into_folder(__get_file_list(sys.argv[1]))
