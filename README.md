# compare-wav

## Description

Module that find duplicated wav files.

## Pip Packages

- numpy
- soundfile

## Terminal Syntax

```
Example with 1 Folder: python compare_wav.py <Folder>
Example with 2 Folders: python compare_wav.py <Folder 1> <Folder 2>
```
